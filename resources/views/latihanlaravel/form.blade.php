@extends('layout.master')

@section('title')
    Buat Account Baru!
@endsection

@section('content')

    <h3> Sign Up Form </h3>
        <form method="post" action="/welcome">
            @csrf
            <label for="fname"> First name: </label><br><br>
            <input type="text" id="fname" name="fname" required><br><br>
            <label for="lname"> Last name: </label><br><br>
            <input type="text" id="lname" name="lname" required>
        
        <p> Gender: </p>
              <input type="radio" id="male" name="Gender" value="Male">
              <label for="male">Male</label><br>
              <input type="radio" id="female" name="Gender" value="Female">
              <label for="female">Female</label><br>
              <input type="radio" id="other" name="Gender" value="Other">
              <label for="other">Other</label><br>

        <p> Nationality: </p>
                <select id="nationality" name="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="america">America</option>
                    <option value="other">Other</option>
                </select>

        <p> Language Spoken </p>
                <input type="checkbox" id="in" name="language" value="Bahasa Indonesia">
                <label for="in"> Bahasa Indonesia </label><br>
                <input type="checkbox" id="en" name="language" value="English">
                <label for="en"> English </label><br>
                <input type="checkbox" id="other" name="language" value="Other">
                <label for="other"> Other </label><br>

        <p> Bio: </p>
                <textarea name="messageorcomment" rows="10" cols="30"></textarea><br><br>
                <input type="submit" value="Sign Up">
        </form>
        
@endsection