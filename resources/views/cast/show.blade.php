@extends('layout.master')

@section('title')
List All Actors {{$cast->name}} 
@endsection

@section('content')

<h3>{{$cast->name}}, ({{$cast->age}})</h3>
<p>{{$cast->bio}}</p>

@endsection