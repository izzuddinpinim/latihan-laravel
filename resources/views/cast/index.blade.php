@extends('layout.master')

@section('title')
List Actors
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
         <tr>
           <td>{{$key + 1}}</td>
           <td>{{$item->name}}</td>
           <td>{{$item->age}}</td>
           <td>{{$item->bio}}</td>
           <td>
            <form action="/cast/{{$item->id}}" method="POST">
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @method('delete')
              @csrf
              <input type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-sm" value="Delete">
            </form>
           </td>
         </tr> 
      @empty
        <tr>
          <td>Data tidak ditemukan</td>
        </tr>
      @endforelse
    </tbody>
</table>

@endsection