<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('latihanlaravel.form');
    }

    public function signup(Request $request){
        $firstname = $request['fname'];
        $lastname = $request['lname'];
        return view('latihanlaravel.welcome', compact('firstname', 'lastname'));
    }
}
